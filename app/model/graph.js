var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var GraphSchema   = new Schema({
	chart_type: String,
	    data: [{
	        series: [Schema.Types.Mixed]
	    }],
	    data_min: Number,
	    data_max: Number,
	    title: String,

});

module.exports = mongoose.model('Graph', GraphSchema);