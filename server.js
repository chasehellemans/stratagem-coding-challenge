var express    = require('express');        
var app        = express();                 
var bodyParser = require('body-parser');
var Graph      = require('./app/model/graph');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public')); 


var port = process.env.PORT || 8080;


var router = express.Router();

router.use(function(req, res, next) {
	next();
});

router.route('/save')

	.post(function(req, res) {
		
		var graph = new Graph();		
		graph.title = req.body.title;  
		graph.data_min = req.body.data_min;
		graph.data_max = req.body.data_max;
		graph.chart_type = req.body.chart_type;
		graph.data = req.body.data;
		graph.save(function(err) {
		if (err) {
				res.send(err);
			}

			res.json({ message: 'graph created!', _id: graph._id });
		});

		
	})

router.route('/get')
	.get(function(req, res) {
		Graph.find(function(err, graphs) {
			if (err) {
				res.send(err);
			}

			res.json(graphs);
		});
	});


router.route('/get/:graph_id')

	.get(function(req, res) {
		Graph.findById(req.params.graph_id, function(err, graph) {
			if (err) {
				res.send(err);
			}
			res.json(graph);
		});
	})

app.use('/graph', router);

app.listen(port);
console.log('Port is listening on: ' + port);

var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://127.0.0.1/database');

exports = module.exports = app; 

