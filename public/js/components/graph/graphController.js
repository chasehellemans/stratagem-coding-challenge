angular.module('GraphApp', ['GraphService'])
    .controller('GraphSaveController', ['$scope', '$location', 'GraphSaveService', function($scope, $location, GraphSaveService) {
        $scope.min = 0;
        $scope.max = 0;
        $scope.title;
        $scope.chart_types = ['Line', 'Scatter', 'Bar']
        $scope.type = $scope.chart_types[0];
        $scope.submit = function() {
            var newGraph = new GraphSaveService({
                data_min: $scope.min,
                data_max: $scope.max,
                title: $scope.title,
                chart_type: $scope.type,
            })
            newGraph.$save().then(function(newGraph) {
                console.log(newGraph);
                $location.path('/show/' + newGraph._id);
            });
        };
    }])
    .controller('GraphDetailController', ['$scope', '$location', '$stateParams', 'GraphGetService',
        function($scope, $location, $stateParams, GraphGetService) {
            GraphGetService.get({
                id: $stateParams._id
            }).$promise.then(function(graph) {
                $scope.id = graph._id;
                console.log(graph);
                $scope.type = graph.chart_type;
                $scope.title = graph.title;
                $scope.min = graph.data_min;
                $scope.max = graph.data_max;
            });
        }
    ])
    .controller('GraphListController', ['$scope', 'GraphGetService',
        function($scope, GraphGetService) {
            GraphGetService.query().$promise.then(function(result) {
                $scope.graphs = result;
            });
        }
    ])
    .filter('unknown', function() {
        return function(input) {
            if (input === undefined) {
                return 'Unknown';
            }
            return input;
        }
    });
