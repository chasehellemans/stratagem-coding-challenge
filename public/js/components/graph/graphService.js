angular.module('GraphService', ['ngResource'])
    .factory('GraphSaveService', ['$resource', graphSaveService])
    .factory('GraphGetService', ['$resource', graphGetService]);

function graphSaveService($resource) {
	return $resource('/graph/save/');
}

function graphGetService($resource) {
    return $resource('/graph/get/:id');
}

