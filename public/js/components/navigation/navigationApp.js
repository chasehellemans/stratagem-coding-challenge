angular.module('NavigationApp', [])
    .controller('NavigationController', function($mdSidenav) {

        this.pages = [{
            title: 'Create a Graph',
            location: 'graph-saver'
        }, {
            title: 'Show a Graph',
            location: 'graph-show'
        }]

        this.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };

    });
