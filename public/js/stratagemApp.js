angular.module('StratagemApp', [
    'highcharts-ng',
    'ui.router',
    'ngResource',
    'ngMaterial',
    'NavigationApp',
    'GraphApp'
])

.config(function($urlMatcherFactoryProvider, $locationProvider, $stateProvider) {

    // Remove HashRouting
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    // Ignore trailing slashes    
    $urlMatcherFactoryProvider.strictMode(false)

    // Routing
    $stateProvider
        .state('graph-saver', {
            url: '/',
            controller: 'GraphSaveController',
            templateUrl: '/js/components/graph/partials/graph-saver.html'
        })
        .state('graph-show', {
            url: '/show',
            controller: 'GraphListController',
            templateUrl: '/js/components/graph/partials/graph-show.html'
        })
        .state('graph-detail', {
            url: '/show/:_id',
            controller: 'GraphDetailController',
            templateUrl: '/js/components/graph/partials/graph-detail.html'
        })
});
